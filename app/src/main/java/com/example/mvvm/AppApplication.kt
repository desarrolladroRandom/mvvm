package com.example.mvvm

import android.app.Application
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.data.database.AppDatabase
import com.example.mvvm.data.network.ConnectivityInterceptor
import com.example.mvvm.data.network.TheMovieDbApiService
import com.example.mvvm.ui.view.homescreen.HomeFragmentViewModelFactory
import com.example.mvvm.ui.view.moviedetail.MovieFragmentViewModelFactory
import com.example.mvvm.ui.view.savedmedia.SavedFragmentViewModelFactory
import com.example.mvvm.ui.view.searchscreen.SearchFragmentViewModelFactory
import com.example.mvvm.ui.view.seasonepisodes.SeasonEpisodeFragmentViewModelFactory
import com.example.mvvm.ui.view.tvshowdetail.TvShowFragmentViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class AppApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@AppApplication))

        bind<ConnectivityInterceptor>() with singleton { ConnectivityInterceptor(instance()) }
        bind() from singleton { TheMovieDbApiService(instance()) }

        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { instance<AppDatabase>().savedTvShowDao() }

        bind<TheMovieDBRepository>() with singleton { TheMovieDBRepository(instance(), instance()) }

        bind() from provider { HomeFragmentViewModelFactory(instance()) }
        bind() from provider { MovieFragmentViewModelFactory(instance()) }
        bind() from provider { TvShowFragmentViewModelFactory(instance()) }
        bind() from provider { SearchFragmentViewModelFactory(instance()) }
        bind() from provider { SavedFragmentViewModelFactory(instance()) }
        bind() from provider { SeasonEpisodeFragmentViewModelFactory(instance()) }

    }
}