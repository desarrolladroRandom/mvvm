package com.example.mvvm.data

import com.example.mvvm.data.dao.SavedMediaDao
import com.example.mvvm.data.network.TheMovieDbApiService
import com.example.mvvm.models.*
import com.example.mvvm.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class TheMovieDBRepository(
    private val theMovieDbApiService: TheMovieDbApiService,
    private val savedMediaDao: SavedMediaDao
) {

    suspend fun getPopularMovies(): Resource<MutableList<Movie>> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getPopularMovies().await()
                return@withContext Resource.success(data.movies)
            } catch (e: Exception) {
                return@withContext Resource.error("", mutableListOf<Movie>())
            }
        }
    }

    suspend fun getMostRatedMovies(): Resource<MutableList<Movie>> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMostRatedMovies().await()
                return@withContext Resource.success(data.movies)
            } catch (e: Exception) {
                return@withContext Resource.error("", mutableListOf<Movie>())
            }
        }
    }

    suspend fun getPopularTvShows(): Resource<MutableList<TvShow>> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getPopularTvShows().await()
                return@withContext Resource.success(data.tvShows)
            } catch (e: Exception) {
                return@withContext Resource.error("", mutableListOf<TvShow>())
            }
        }
    }

    suspend fun getMostRatedTvShows(): Resource<MutableList<TvShow>> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMostRatedTvShows().await()
                return@withContext Resource.success(data.tvShows)
            } catch (e: Exception) {
                return@withContext Resource.error("", mutableListOf<TvShow>())
            }
        }
    }

    suspend fun getMovieDetail(movieId: Int): Resource<MovieDetail> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMovieDetails(movieId).await()
                if (savedMediaDao.checkIfMediaIsSaved(movieId) == 1) data.saved = true
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", MovieDetail())
            }
        }
    }

    suspend fun getMovieDetailCredits(movieId: Int): Resource<Credits> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMovieDetailCredits(movieId).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", Credits())
            }
        }
    }

    suspend fun getMovieDetailRecommendations(movieId: Int): Resource<MoviesResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMovieDetailRecommendations(movieId).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", MoviesResponse())
            }
        }
    }

    suspend fun getMovieDetailSimilar(movieId: Int): Resource<MoviesResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMovieDetailSimilar(movieId).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", MoviesResponse())
            }
        }
    }

    suspend fun getTvShowDetail(serieId: Int): Resource<TvShowDetail> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTvShowDetails(serieId).await()
                if (savedMediaDao.checkIfMediaIsSaved(serieId) == 1) data.saved = true
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", TvShowDetail())
            }
        }
    }

    suspend fun getTvShowDetailCredits(serieId: Int): Resource<Credits> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTvShowDetailCredits(serieId).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", Credits())
            }
        }
    }

    suspend fun getTvShowDetailRecommendations(serieId: Int): Resource<TvShowResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTvShowDetailRecommendations(serieId).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", TvShowResponse())
            }
        }
    }

    suspend fun getTvShowDetailSimilar(serieId: Int): Resource<TvShowResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTvShowDetailSimilar(serieId).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", TvShowResponse())
            }
        }
    }

    suspend fun getMultiSearch(searchString: String, page: Int): Resource<SearchResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getMultiSearch(searchString, page).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", SearchResponse())
            }
        }
    }

    suspend fun getTvShowAiringToday(): Resource<TvShowResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTvShowAiringToday().await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", TvShowResponse())
            }
        }
    }

    suspend fun saveMediaToPlaylist(saveTvShow: SavedMedia) {
        return withContext(Dispatchers.IO) {
            savedMediaDao.saveMediaToList(saveTvShow)
        }
    }

    suspend fun getMediaInSelectedPlaylist(playlist: String): MutableList<SavedMedia> {
        return withContext(Dispatchers.IO) {
            return@withContext savedMediaDao.getSavedMediaByListId(playlist)
        }
    }

    suspend fun getTrendingMediaByTime(type: String, time: String): Resource<TrendingResponse> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTrendingMediaByTime(type, time).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", TrendingResponse())
            }
        }
    }

    suspend fun getTvShowSeasonEpisode(tv_id: Int, season_number: Int): Resource<SeasonEpisodes> {
        return withContext(Dispatchers.IO) {
            try {
                var data = theMovieDbApiService.getTvShowSeasonEpisode(tv_id, season_number).await()
                return@withContext Resource.success(data)
            } catch (e: Exception) {
                return@withContext Resource.error("$e", SeasonEpisodes())
            }
        }
    }
}