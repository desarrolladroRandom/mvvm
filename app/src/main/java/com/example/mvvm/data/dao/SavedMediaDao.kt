package com.example.mvvm.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.TypeConverter
import com.example.mvvm.models.SavedMedia

@Dao
interface SavedMediaDao {
    @Insert()
    fun saveMediaToList(savedMedia: SavedMedia)

    @Query("select * from user_saved_media where itemId =:mediaId limit 1")
    fun getSavedMediaById(mediaId: Int): SavedMedia

    @Query("select * from user_saved_media where listName =:listName")
    fun getSavedMediaByListId(listName: String): MutableList<SavedMedia>

    @Query("select * from user_saved_media")
    fun getAllSavedMedia(): MutableList<SavedMedia>

    @Query("select COUNT(*) from user_saved_media where itemId =:mediaId limit 1")
    fun checkIfMediaIsSaved(mediaId: Int): Int
}

class Converter {
    @TypeConverter
    fun genreIdsToString(genreIds: MutableList<Int>): String {
        return genreIds.toString()
    }

    @TypeConverter
    fun stringToGenreIds(genreIdsString: String): MutableList<Int> {
        return genreIdsString
            .replace("[", "")
            .replace("]", "")
            .replace(" ", "")
            .split(",")
            .map { it.toInt() }
            .toMutableList()
    }

    @TypeConverter
    fun originCountriesToString(originCountries: MutableList<String>): String {
        return originCountries.toString()
    }

    @TypeConverter
    fun stringToOriginCountries(originCountriesString: String): MutableList<String> {
        return originCountriesString
            .replace("[", "")
            .replace("]", "")
            .split(",")
            .map { it }.toMutableList()
    }

}