package com.example.mvvm.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.mvvm.data.dao.Converter
import com.example.mvvm.data.dao.SavedMediaDao
import com.example.mvvm.models.SavedMedia

@Database(entities = [SavedMedia::class], version = 1)
@TypeConverters(Converter::class)

abstract class AppDatabase : RoomDatabase() {

    abstract fun savedTvShowDao(): SavedMediaDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance
            ?: synchronized(LOCK) {
                instance ?: builDatabase(context).also { instance = it }
            }

        private fun builDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java, "tmdb.db"
        ).build()
    }
}