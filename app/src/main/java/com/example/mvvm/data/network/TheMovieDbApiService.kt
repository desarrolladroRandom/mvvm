package com.example.mvvm.data.network

import com.example.mvvm.BuildConfig
import com.example.mvvm.MainActivity
import com.example.mvvm.models.*
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDbApiService {
    @GET("movie/popular/")
    fun getPopularMovies(): Deferred<MoviesResponse>

    @GET("movie/top_rated/")
    fun getMostRatedMovies(): Deferred<MoviesResponse>

    @GET("tv/popular/")
    fun getPopularTvShows(): Deferred<TvShowResponse>

    @GET("tv/top_rated/")
    fun getMostRatedTvShows(): Deferred<TvShowResponse>

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") movieId: Int): Deferred<MovieDetail>

    @GET("movie/{movie_id}/credits")
    fun getMovieDetailCredits(@Path("movie_id") movieId: Int): Deferred<Credits>

    @GET("movie/{movie_id}/recommendations")
    fun getMovieDetailRecommendations(@Path("movie_id") movieId: Int): Deferred<MoviesResponse>

    @GET("movie/{movie_id}/similar")
    fun getMovieDetailSimilar(@Path("movie_id") movieId: Int): Deferred<MoviesResponse>

    @GET("tv/{tvShow_id}")
    fun getTvShowDetails(@Path("tvShow_id") movieId: Int): Deferred<TvShowDetail>

    @GET("tv/{tvShow_id}/credits")
    fun getTvShowDetailCredits(@Path("tvShow_id") movieId: Int): Deferred<Credits>

    @GET("tv/{tvShow_id}/recommendations")
    fun getTvShowDetailRecommendations(@Path("tvShow_id") movieId: Int): Deferred<TvShowResponse>

    @GET("tv/{tvShow_id}/similar")
    fun getTvShowDetailSimilar(@Path("tvShow_id") movieId: Int): Deferred<TvShowResponse>

    @GET("search/multi")
    fun getMultiSearch(@Query("query") search: String, @Query("page") page: Int): Deferred<SearchResponse>

    @GET("tv/airing_today")
    fun getTvShowAiringToday(): Deferred<TvShowResponse>

    @GET("trending/{media_type}/{time_window}")
    fun getTrendingMediaByTime(@Path("media_type") type: String, @Path("time_window") time: String): Deferred<TrendingResponse>

    @GET("tv/{tv_id}/season/{season_number}")
    fun getTvShowSeasonEpisode(@Path("tv_id") tv_id: Int, @Path("season_number") season_number: Int): Deferred<SeasonEpisodes>

    companion object {
        operator fun invoke(connectivityInterceptor: ConnectivityInterceptor): TheMovieDbApiService {
            val requestInterceptor = Interceptor { chain ->
                val url = chain.request().url().newBuilder().addQueryParameter(
                        "api_key",
                        BuildConfig.TMDB_KEY
                ).build()
                val request = chain.request().newBuilder().url(url).build()
                return@Interceptor chain.proceed(request)
            }

            //val logginInterceptor = HttpLoggingInterceptor()
            //logginInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                    .addInterceptor(requestInterceptor)
                    //.addInterceptor(logginInterceptor)
                    .addInterceptor(connectivityInterceptor)
                    .build()


            return Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.TMDB_URL).addCallAdapterFactory(CoroutineCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create()).build().create(TheMovieDbApiService::class.java)
        }
    }
}






