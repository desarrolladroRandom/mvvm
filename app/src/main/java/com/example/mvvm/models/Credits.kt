package com.example.mvvm.models

import com.google.gson.annotations.SerializedName

data class Credits(
    @SerializedName("cast")
    val cast: MutableList<Cast> = mutableListOf(),
    @SerializedName("crew")
    val crew: MutableList<Crew> = mutableListOf(),
    @SerializedName("id")
    val id: Int = 0
)