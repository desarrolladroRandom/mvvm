package com.example.mvvm.models

import androidx.room.Embedded
import androidx.room.Entity


@Entity(tableName = "user_saved_media", primaryKeys = ["listName", "itemId"])
data class SavedMedia(
        var itemId: Int = 0,
        var listName: String = "",
        @Embedded(prefix = "_savedItem")
        var savedItem: MediaItem?
)

data class MediaItem(
        var adult: Boolean,
        var backdropPath: String,
        var firstAirDate: String,
        var genreIds: MutableList<Int>,
        var id: Int,
        var mediaType: String,
        var name: String,
        var originCountry: MutableList<String>,
        var originalLanguage: String,
        var originalName: String,
        var originalTitle: String,
        var overview: String,
        var popularity: Double,
        var posterPath: String,
        var releaseDate: String,
        var title: String,
        var voteAverage: Double,
        var voteCount: Int
)