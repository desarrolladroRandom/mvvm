package com.example.mvvm.models

import com.google.gson.annotations.SerializedName

data class SearchResponse(
        @SerializedName("page")
        val page: Int = 0,
        @SerializedName("results")
        val results: MutableList<SearchResult> = mutableListOf(),
        @SerializedName("total_pages")
        val totalPages: Int = 0,
        @SerializedName("total_results")
        val totalResults: Int = 0
)