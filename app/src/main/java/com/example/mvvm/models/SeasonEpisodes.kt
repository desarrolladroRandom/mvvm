package com.example.mvvm.models

import com.google.gson.annotations.SerializedName

data class SeasonEpisodes(
    @SerializedName("_id")
    val _id: String = "",
    @SerializedName("air_date")
    val airDate: String = "",
    @SerializedName("episodes")
    val episodes: MutableList<Episode> = mutableListOf(),
    @SerializedName("id")
    val id: Int = 0,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("overview")
    val overview: String = "",
    @SerializedName("poster_path")
    val posterPath: String = "",
    @SerializedName("season_number")
    val seasonNumber: Int = 0
)