package com.example.mvvm.models

import com.google.gson.annotations.SerializedName

data class TvShowDetail(
        @SerializedName("backdrop_path")
        val backdropPath: String = "",
        @SerializedName("created_by")
        val createdBy: MutableList<CreatedBy> = mutableListOf(),
        @SerializedName("episode_run_time")
        val episodeRunTime: MutableList<Int> = mutableListOf(),
        @SerializedName("first_air_date")
        val firstAirDate: String = "",
        @SerializedName("genres")
        val genres: MutableList<Genre> = mutableListOf(),
        @SerializedName("homepage")
        val homepage: String = "",
        @SerializedName("id")
        val id: Int = 0,
        @SerializedName("in_production")
        val inProduction: Boolean=false,
        @SerializedName("languages")
        val languages: MutableList<String> = mutableListOf(),
        @SerializedName("last_air_date")
        val lastAirDate: String="",
        @SerializedName("last_episode_to_air")
        val lastEpisodeToAir: LastEpisodeToAir=LastEpisodeToAir(),
        @SerializedName("name")
        val name: String="",
        @SerializedName("networks")
        val networks: MutableList<Network> = mutableListOf(),
        @SerializedName("next_episode_to_air")
        val nextEpisodeToAir: NextEpisodeToAir = NextEpisodeToAir(),
        @SerializedName("number_of_episodes")
        val numberOfEpisodes: Int=0,
        @SerializedName("number_of_seasons")
        val numberOfSeasons: Int=0,
        @SerializedName("origin_country")
        val originCountry: MutableList<String> = mutableListOf(),
        @SerializedName("original_language")
        val originalLanguage: String="",
        @SerializedName("original_name")
        val originalName: String="",
        @SerializedName("overview")
        val overview: String="",
        @SerializedName("popularity")
        val popularity: Double=0.0,
        @SerializedName("poster_path")
        val posterPath: String="",
        @SerializedName("production_companies")
        val productionCompanies: MutableList<ProductionCompany> = mutableListOf(),
        @SerializedName("seasons")
        val seasons: MutableList<Season> = mutableListOf(),
        @SerializedName("status")
        val status: String="",
        @SerializedName("type")
        val type: String="",
        @SerializedName("vote_average")
        val voteAverage: Double=0.0,
        @SerializedName("vote_count")
        val voteCount: Int=0,
        var saved: Boolean = false
)