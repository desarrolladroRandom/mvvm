package com.example.mvvm.models

import com.google.gson.annotations.SerializedName

data class TvShowResponse(
    @SerializedName("page")
    val page: Int=0,
    @SerializedName("results")
    val tvShows: MutableList<TvShow> = mutableListOf(),
    @SerializedName("total_pages")
    val totalPages: Int=0,
    @SerializedName("total_results")
    val totalResults: Int=0
)