package com.example.mvvm.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.models.Movie
import com.example.mvvm.models.TvShow
import kotlinx.android.synthetic.main.adapter_backdrop_layout.view.*

class BackdropViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var cv_backdrop: CardView = itemView.cv_backdrop
    var img_backdrop: ImageView = itemView.img_backdrop
    var txt_backdrop = itemView.txt_backdrop_title as TextView
}

class BackdropAdapter(context: Context?, var airingToday: MutableList<TvShow>) :
    RecyclerView.Adapter<BackdropViewHolder>() {
    internal var context: Context? = context
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BackdropViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_backdrop_layout, parent, false)
        return BackdropViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holder: BackdropViewHolder, position: Int) {
        var tvShow = airingToday[position]

        if (tvShow.backdropPath.isNullOrBlank())
            Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.img_backdrop)
        else
            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w500" + tvShow.backdropPath)
                .placeholder(R.drawable.poster_placeholder)
                .into(holder.img_backdrop)

        holder.txt_backdrop.text = tvShow.name

        holder.cv_backdrop.setOnClickListener {
            var navOptions = NavOptions.Builder().apply {
                setEnterAnim(R.anim.slide_in_right)
                setExitAnim(R.anim.slide_out_left)
                setPopEnterAnim(R.anim.slide_in_left)
                setPopExitAnim(R.anim.slide_out_right)
            }

            var args = bundleOf("tvShow_id" to tvShow.id)
            Navigation.findNavController(it).navigate(R.id.tvShowDetailFragment, args, navOptions.build())
        }
        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = airingToday.size

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}