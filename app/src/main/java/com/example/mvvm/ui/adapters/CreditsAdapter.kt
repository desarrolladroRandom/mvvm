package com.example.mvvm.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.models.Cast
import kotlinx.android.synthetic.main.adapter_cast_layout.view.*

class CreditsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var cast_profile: ImageView = itemView.img_cast_profile
    var cast_name: TextView = itemView.txt_cast_name
}

class CreditsAdapter(context: Context?, var cast: MutableList<Cast>) : RecyclerView.Adapter<CreditsViewHolder>() {
    internal var context: Context? = context
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CreditsViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_cast_layout, parent, false)
        return CreditsViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holder: CreditsViewHolder, position: Int) {
        var actor = cast[position]

        holder.cast_name.text = actor.name

        if (actor.profilePath.isNullOrBlank())
            Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.cast_profile)
        else
            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w92" + actor.profilePath)
                .placeholder(R.drawable.poster_placeholder)
                .into(holder.cast_profile)

        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = cast.size

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}