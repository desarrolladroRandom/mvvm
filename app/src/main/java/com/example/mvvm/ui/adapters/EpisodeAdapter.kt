package com.example.mvvm.ui.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.models.Episode
import kotlinx.android.synthetic.main.adapter_episode_layout.view.*

class EpisodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var episodeItem: ConstraintLayout = itemView.episode_item
    var episodeName: TextView = itemView.episode_name
    var epiodeOverview: TextView = itemView.episode_overview
    var episodePoster: ImageView = itemView.episode_poster
}

class EpisodeAdapter(context: Context?, var episodes: MutableList<Episode>) :
    RecyclerView.Adapter<EpisodeViewHolder>() {
    internal var context: Context? = context
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_episode_layout, parent, false)
        return EpisodeViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        var episode = episodes[position]
        holder.episodeName.text = episode.name

        if (episode.overview.isNullOrBlank())
            holder.epiodeOverview.text = "No description found..."
        else
            holder.epiodeOverview.text = episode.overview

        if (episode.stillPath.isNullOrBlank())
            Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.episodePoster)
        else
            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w300" + episode.stillPath)
                .placeholder(R.drawable.poster_placeholder)
                .into(holder.episodePoster)

        if (position % 2 == 0)
            holder.episodeItem.setBackgroundColor(ContextCompat.getColor(context!!, R.color.customDarkWhite))

        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = episodes.size

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}