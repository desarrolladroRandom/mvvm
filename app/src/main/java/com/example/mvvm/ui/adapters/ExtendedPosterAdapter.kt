package com.example.mvvm.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.models.Episode
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.models.SearchResult
import com.example.mvvm.models.TrendingResult
import com.example.mvvm.utils.getGenreIdToString
import kotlinx.android.synthetic.main.adapter_search_item.view.*

class ExtendedPosterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var searchLayout: ConstraintLayout = itemView.search_item_layout
    var searchTitle: TextView = itemView.search_title
    var searchGenres: TextView = itemView.search_genres
    var searchOverview: TextView = itemView.search_overview
    var searchPoster: ImageView = itemView.search_poster
    var searchNote: TextView = itemView.search_note
}

class ExtendedPosterAdapter<T>(context: Context?, var searchItems: MutableList<T>) :
    RecyclerView.Adapter<ExtendedPosterViewHolder>() {
    internal var context: Context? = context
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExtendedPosterViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_search_item, parent, false)
        return ExtendedPosterViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holder: ExtendedPosterViewHolder, position: Int) {
        var result = searchItems[position]

        if (result is SearchResult) {
            if (result.mediaType == "movie" || result.mediaType == "tv") {
                if (result.mediaType == "movie")
                    holder.searchTitle.text = result.title
                else
                    holder.searchTitle.text = result.name

                holder.searchNote.text = result.voteAverage.toString()
                holder.searchGenres.text = result.genreIds.map { getGenreIdToString(it) }.take(3).toString()
                holder.searchOverview.text = result.overview

                if (result.posterPath.isNullOrBlank())
                    Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.searchPoster)
                else
                    Glide.with(context!!)
                        .load("https://image.tmdb.org/t/p/w300" + result.posterPath)
                        .placeholder(R.drawable.poster_placeholder)
                        .into(holder.searchPoster)

            } else if (result.mediaType == "person") {
                if (result.profilePath.isNullOrBlank())
                    Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.searchPoster)
                else
                    Glide.with(context!!)
                        .load("https://image.tmdb.org/t/p/w300" + result.profilePath)
                        .placeholder(R.drawable.poster_placeholder)
                        .into(holder.searchPoster)
                holder.searchTitle.text = result.name
                holder.searchOverview.text = result.knownFor.map {
                    if (it.mediaType == "tv")
                        it.name
                    else
                        it.title
                }.toString()
                holder.searchNote.visibility = View.GONE
            }

            holder.searchLayout.setOnClickListener {
                var navOptions = NavOptions.Builder().apply {
                    setEnterAnim(R.anim.slide_in_right)
                    setExitAnim(R.anim.slide_out_left)
                    setPopEnterAnim(R.anim.slide_in_left)
                    setPopExitAnim(R.anim.slide_out_right)
                }
                if (result.mediaType == "movie") {
                    var args = bundleOf("movie_id" to result.id)
                    Navigation.findNavController(it).navigate(R.id.movieDetailFragment, args, navOptions.build())
                    Navigation.findNavController(it).navigate(R.id.movieDetailFragment, args)
                }

                if (result.mediaType == "tv") {
                    var args = bundleOf("tvShow_id" to result.id)
                    Navigation.findNavController(it).navigate(R.id.tvShowDetailFragment, args, navOptions.build())
                    Navigation.findNavController(it).navigate(R.id.tvShowDetailFragment, args)
                }
            }
        }

        if (result is SavedMedia) {
            if (result.savedItem?.mediaType == "movie" || result.savedItem?.mediaType == "tv") {
                if (result.savedItem?.mediaType == "movie")
                    holder.searchTitle.text = result.savedItem?.title
                else
                    holder.searchTitle.text = result.savedItem?.name

                holder.searchNote.text = result.savedItem?.voteAverage.toString()
                holder.searchGenres.text =
                    result.savedItem?.genreIds?.map { getGenreIdToString(it) }?.take(3).toString()
                holder.searchOverview.text = result.savedItem?.overview

                if (result.savedItem?.posterPath.isNullOrBlank())
                    Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.searchPoster)
                else
                    Glide.with(context!!)
                        .load("https://image.tmdb.org/t/p/w300" + result.savedItem?.posterPath)
                        .placeholder(R.drawable.poster_placeholder)
                        .into(holder.searchPoster)
            }

            holder.searchLayout.setOnClickListener {
                var navOptions = NavOptions.Builder().apply {
                    setEnterAnim(R.anim.slide_in_right)
                    setExitAnim(R.anim.slide_out_left)
                    setPopEnterAnim(R.anim.slide_in_left)
                    setPopExitAnim(R.anim.slide_out_right)
                }
                if (result.savedItem?.mediaType == "movie") {
                    var args = bundleOf("movie_id" to result.savedItem?.id)
                    Navigation.findNavController(it).navigate(R.id.movieDetailFragment, args, navOptions.build())
                }

                if (result.savedItem?.mediaType == "tv") {
                    var args = bundleOf("tvShow_id" to result.savedItem?.id)
                    Navigation.findNavController(it).navigate(R.id.tvShowDetailFragment, args, navOptions.build())
                }
            }
        }

        if (result is TrendingResult) {
            if (result.title.isNullOrBlank())
                holder.searchTitle.text = result.name
            else
                holder.searchTitle.text = result.title

            holder.searchGenres.text = result.genreIds.map { getGenreIdToString(it) }.toString()
            holder.searchNote.text = result.voteAverage.toString()
            holder.searchOverview.text = result.overview

            if (result.posterPath.isNullOrBlank())
                Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.searchPoster)
            else
                Glide.with(context!!)
                    .load("https://image.tmdb.org/t/p/w300" + result.posterPath)
                    .placeholder(R.drawable.poster_placeholder)
                    .into(holder.searchPoster)

            holder.searchLayout.setOnClickListener {
                var navOptions = NavOptions.Builder().apply {
                    setEnterAnim(R.anim.slide_in_right)
                    setExitAnim(R.anim.slide_out_left)
                    setPopEnterAnim(R.anim.slide_in_left)
                    setPopExitAnim(R.anim.slide_out_right)
                }
                if (result.name.isNullOrBlank()) {
                    var args = bundleOf("movie_id" to result.id)
                    Navigation.findNavController(it).navigate(R.id.movieDetailFragment, args, navOptions.build())
                } else {
                    var args = bundleOf("tvShow_id" to result.id)
                    Navigation.findNavController(it).navigate(R.id.tvShowDetailFragment, args, navOptions.build())
                }
            }
        }

        if (result is Episode) {
            holder.searchTitle.text = result.name
            holder.searchOverview.text = result.overview
            holder.searchNote.text = result.voteAverage.toString()
            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w300" + result.stillPath)
                .placeholder(R.drawable.poster_placeholder)
                .into(holder.searchPoster)
        }

        if (position % 2 == 0)
            holder.searchLayout.setBackgroundColor(ContextCompat.getColor(context!!, R.color.customDarkWhite))

        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = searchItems.size

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}