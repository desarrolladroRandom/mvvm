package com.example.mvvm.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.models.Season
import com.example.mvvm.ui.view.tvshowdetail.TvShowDetailObj.tvShowDetailInfo
import kotlinx.android.synthetic.main.adapter_season_layout.view.*

class SeasonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var seasonItem: ConstraintLayout = itemView.season_item
    var seasonName: TextView = itemView.season_name
    var seasonEpisodes: TextView = itemView.season_episodes
    var seasonOverview: TextView = itemView.season_overview
    var seasonPoster: ImageView = itemView.season_poster
}

class SeasonAdapter(context: Context?, var seasons: MutableList<Season>) : RecyclerView.Adapter<SeasonViewHolder>() {
    internal var context: Context? = context
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_season_layout, parent, false)
        return SeasonViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holder: SeasonViewHolder, position: Int) {
        var season = seasons[position]
        holder.seasonName.text = season.name
        holder.seasonEpisodes.text = "${season.episodeCount} Episodes"
        holder.seasonOverview.text = season.overview

        if (season.posterPath.isNullOrBlank())
            Glide.with(context!!).load(R.drawable.poster_placeholder).into(holder.seasonPoster)
        else
            Glide.with(context!!)
                .load("https://image.tmdb.org/t/p/w300" + season.posterPath)
                .placeholder(R.drawable.poster_placeholder)
                .into(holder.seasonPoster)

        if (position % 2 == 0)
            holder.seasonItem.setBackgroundColor(ContextCompat.getColor(context!!, R.color.customDarkWhite))


        holder.seasonItem.setOnClickListener {
            var navOptions = NavOptions.Builder().apply {
                setEnterAnim(R.anim.slide_in_right)
                setExitAnim(R.anim.slide_out_left)
                setPopEnterAnim(R.anim.slide_in_left)
                setPopExitAnim(R.anim.slide_out_right)
            }
            var args = bundleOf("tvShowId" to tvShowDetailInfo.value?.id, "seasonNum" to position)
            Navigation.findNavController(it).navigate(R.id.seasonEpisodesFragment, args, navOptions.build())
        }
        setAnimation(holder.itemView, position)
    }

    override fun getItemCount(): Int = seasons.size

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}