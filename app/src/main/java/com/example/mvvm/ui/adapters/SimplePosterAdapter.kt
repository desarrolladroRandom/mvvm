package com.example.mvvm.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.navigation.NavOptions
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.mvvm.R
import com.example.mvvm.models.Movie
import com.example.mvvm.models.TvShow
import kotlinx.android.synthetic.main.adapter_poster_layout.view.*


class SimplePosterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var poster_card = itemView.cv_poster as CardView
    var poster_image = itemView.img_poster as ImageView
    var poster_title = itemView.txt_poster_title as TextView
}

class SimplePosterAdapter<T>(context: Context?, var items: MutableList<T>) :
    RecyclerView.Adapter<SimplePosterViewHolder>() {
    private var context: Context? = context
    private var lastPosition = -1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimplePosterViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.adapter_poster_layout, parent, false)
        return SimplePosterViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holderSimple: SimplePosterViewHolder, position: Int) {
        var item = items[position]

        if (item is Movie) {
            if (item.posterPath.isNullOrBlank())
                Glide.with(context!!).load(R.drawable.poster_placeholder).into(holderSimple.poster_image)
            else
                Glide.with(context!!)
                    .load("https://image.tmdb.org/t/p/w300" + item.posterPath)
                    .placeholder(R.drawable.poster_placeholder)
                    .into(holderSimple.poster_image)
            holderSimple.poster_title.text = item.title
        }

        if (item is TvShow) {
            if (item.posterPath.isNullOrBlank())
                Glide.with(context!!).load(R.drawable.poster_placeholder).into(holderSimple.poster_image)
            else
                Glide.with(context!!)
                    .load("https://image.tmdb.org/t/p/w300" + item.posterPath)
                    .placeholder(R.drawable.poster_placeholder)
                    .into(holderSimple.poster_image)

            holderSimple.poster_title.text = item.name
        }

        holderSimple.poster_card.setOnClickListener {
            var navOptions = NavOptions.Builder().apply {
                setEnterAnim(R.anim.slide_in_right)
                setExitAnim(R.anim.slide_out_left)
                setPopEnterAnim(R.anim.slide_in_left)
                setPopExitAnim(R.anim.slide_out_right)
            }
            if (item is Movie) {
                var args = bundleOf("movie_id" to item.id)
                findNavController(it).navigate(R.id.movieDetailFragment, args, navOptions.build())
            }

            if (item is TvShow) {
                var args = bundleOf("tvShow_id" to item.id)
                findNavController(it).navigate(R.id.tvShowDetailFragment, args, navOptions.build())
            }
        }

        //setAnimation(holderSimple.itemView,position)
    }

    override fun getItemCount() = items.size

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, R.anim.slide_in_right)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}