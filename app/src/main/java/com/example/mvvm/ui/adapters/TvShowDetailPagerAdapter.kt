package com.example.mvvm.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class TvShowDetailPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    var fragmentList = mutableListOf<Fragment>()
    var fragmentTitles = mutableListOf<String>()

    override fun getItem(position: Int): Fragment = fragmentList[position]
    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence = fragmentTitles[position]

    fun addFragment(fragment: Fragment, fragmentTitle: String) {
        fragmentList.add(fragment)
        fragmentTitles.add(fragmentTitle)
    }
}