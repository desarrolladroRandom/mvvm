package com.example.mvvm.ui.view.homescreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvm.R
import com.example.mvvm.models.Movie
import com.example.mvvm.models.TvShow
import com.example.mvvm.ui.adapters.BackdropAdapter
import com.example.mvvm.ui.adapters.SimplePosterAdapter
import com.example.mvvm.utils.Resource
import com.example.mvvm.utils.ScopedFragment
import com.example.mvvm.utils.buildErro404Alert
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class HomeFragment : ScopedFragment(), KodeinAware {
    override val kodein by kodein()

    private val homeFragmentViewModelFactory: HomeFragmentViewModelFactory by instance()
    private lateinit var homeFragmentViewModel: HomeFragmentViewModel

    companion object {
        var popularMovies = mutableListOf<Movie>()
        var popularTvShows = mutableListOf<TvShow>()
        var airingToday = mutableListOf<TvShow>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeFragmentViewModel =
            ViewModelProviders.of(activity!!, homeFragmentViewModelFactory).get(HomeFragmentViewModel::class.java)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        homeFragmentViewModel.popularMovies.observe(this@HomeFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading most rated movies ${it.message}")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    rv_popular_movies.apply {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = SimplePosterAdapter(context, it.data)
                        popularMovies = it.data
                    }
                }
            }
        })

        homeFragmentViewModel.popularTvShows.observe(this@HomeFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading popular tv shows  ${it.message}")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    rv_popular_series.apply {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = SimplePosterAdapter(context, it.data)
                        popularTvShows = it.data
                    }
                }
            }
        })

        homeFragmentViewModel.airingToday.observe(this@HomeFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading popular tv shows  ${it.message}")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    rv_airing_today.apply {
                        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                        adapter = BackdropAdapter(context, it.data.tvShows)
                        airingToday = it.data.tvShows
                    }

                    // ESTO ES TEMPORAL
                    loaded_home.visibility = View.VISIBLE
                    loading_home.visibility = View.GONE
                }
            }
        })

        if (popularMovies.isEmpty() && popularTvShows.isEmpty() && airingToday.isEmpty()) {
            bindUI()
        } else {
            initializeViews()
        }

    }

    private fun bindUI() = launch {
        loaded_home.visibility = View.GONE
        loading_home.visibility = View.VISIBLE

        homeFragmentViewModel.getPopularMovies()
        homeFragmentViewModel.getPopularTvShows()
        //homeFragmentViewModel.getMostRatedMovies()
        //homeFragmentViewModel.getMostRatedTvShows()
        homeFragmentViewModel.getTvShowAiringToday()
    }

    private fun initializeViews() {
        rv_popular_movies.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = SimplePosterAdapter(context, popularMovies)
        }
        rv_popular_series.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = SimplePosterAdapter(context, popularTvShows)
        }
        rv_airing_today.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = SimplePosterAdapter(context, airingToday)
        }
        loaded_home.visibility = View.VISIBLE
    }
}
