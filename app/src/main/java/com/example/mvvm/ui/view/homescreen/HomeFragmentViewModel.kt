package com.example.mvvm.ui.view.homescreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.models.Movie
import com.example.mvvm.models.TvShow
import com.example.mvvm.models.TvShowResponse
import com.example.mvvm.utils.Resource

class HomeFragmentViewModel(private val theMovieDBRepository: TheMovieDBRepository) : ViewModel() {
    val airingToday = MutableLiveData<Resource<TvShowResponse>>()
    val popularMovies = MutableLiveData<Resource<MutableList<Movie>>>()
    val mostRatedMovies = MutableLiveData<Resource<MutableList<Movie>>>()
    val popularTvShows = MutableLiveData<Resource<MutableList<TvShow>>>()
    val mostRatedTvShows = MutableLiveData<Resource<MutableList<TvShow>>>()

    suspend fun getPopularMovies() {
        popularMovies.value = theMovieDBRepository.getPopularMovies()
    }

    suspend fun getMostRatedMovies() {
        mostRatedMovies.value = theMovieDBRepository.getMostRatedMovies()
    }

    suspend fun getPopularTvShows() {
        popularTvShows.value = theMovieDBRepository.getPopularTvShows()
    }

    suspend fun getMostRatedTvShows() {
        mostRatedTvShows.value = theMovieDBRepository.getMostRatedTvShows()
    }

    suspend fun getTvShowAiringToday() {
        airingToday.value = theMovieDBRepository.getTvShowAiringToday()
    }
}