package com.example.mvvm.ui.view.homescreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvm.data.TheMovieDBRepository

class HomeFragmentViewModelFactory(private val theMovieDBRepository: TheMovieDBRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeFragmentViewModel(theMovieDBRepository) as T
    }
}