package com.example.mvvm.ui.view.moviedetail

import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.mvvm.MainActivity
import com.example.mvvm.R
import com.example.mvvm.models.Credits
import com.example.mvvm.models.Movie
import com.example.mvvm.models.MovieDetail
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.ui.adapters.CreditsAdapter
import com.example.mvvm.ui.adapters.SimplePosterAdapter
import com.example.mvvm.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class MovieDetailFragment : ScopedFragment(), KodeinAware {
    override val kodein by kodein()

    private val movieFragmentViewModelFactory: MovieFragmentViewModelFactory by instance()
    private lateinit var movieDetailFragmentViewModel: MovieDetailFragmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_movie_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        movieDetailFragmentViewModel = ViewModelProviders.of(this, movieFragmentViewModelFactory)
                .get(MovieDetailFragmentViewModel::class.java)

        val movieId = arguments!!.getInt("movie_id")

        bindUI(movieId)
    }

    private fun bindUI(movieId: Int) = launch {
        movieDetailFragmentViewModel.updateMovieDetail(movieId)
        movieDetailFragmentViewModel.movieDetail.observe(this@MovieDetailFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading movie detail ${it.message}")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> bindDetailsUI(it.data)
            }
        })

        movieDetailFragmentViewModel.updateMovieDetailCredits(movieId)
        movieDetailFragmentViewModel.movieDetailCredits.observe(this@MovieDetailFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading credits")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> bindCreditsUI(it.data)
            }
        })

        movieDetailFragmentViewModel.updateMovieDetailRecommendations(movieId)
        movieDetailFragmentViewModel.movieDetailRecommendations.observe(
                this@MovieDetailFragment,
                Observer {
                    if (it == null) return@Observer
                    when (it.status) {
                        Resource.Status.ERROR -> {
                            //Log.e("XXXX", "Error loading movie detail")
                            buildErro404Alert(context).show()
                        }
                        Resource.Status.SUCCESS -> {
                            bindRecommendationsUI(it.data.movies)
                            //TEMPORAL S'HA DE CANVIAR
                            loaded_movie_detail.visibility = View.VISIBLE
                            loading_movie_detail.visibility = View.GONE
                        }
                    }

                })
    }

    private fun bindDetailsUI(movieDetail: MovieDetail) {
        movie_detail_title.isSelected = true
        movie_detail_title.text = movieDetail.title
        movie_detail_overview.text = movieDetail.overview
        movie_detail_date.text = movieDetail.releaseDate
        movie_detail_votes.text = "${movieDetail.voteCount} votes"
        movie_detail_note.text = movieDetail.voteAverage.toString()
        movie_detail_genres.text = movieDetail.genres.map { it.name }.toString()


        if (movieDetail.saved)
            img_save_movie.setBackgroundResource(R.drawable.ic_baseline_bookmark)

        var tmdbHomePage = "https://www.imdb.com/title/${movieDetail.imdbId}"
        if (movieDetail.homepage != null) tmdbHomePage = movieDetail.homepage

        movie_detail_homepage.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        movie_detail_homepage.text = tmdbHomePage

        movie_detail_homepage.setOnClickListener {
            var i = Intent(Intent.ACTION_VIEW, Uri.parse(tmdbHomePage))
            startActivity(i)
        }

        movie_detail_duration.text = "${movieDetail.runtime} min."

        Glide.with(context!!).load("https://image.tmdb.org/t/p/w500" + movieDetail.posterPath)
                .into(movie_detail_poster)
        Glide.with(context!!).load("https://image.tmdb.org/t/p/w500" + movieDetail.backdropPath)
                .into(movie_detail_backdrop)

        img_share_movie.setOnClickListener {
            val i = Intent(Intent.ACTION_SEND)
            i.type = "text/plain"
            i.putExtra(Intent.EXTRA_SUBJECT, "Echale un vistazo a esta pelicula :")
            i.putExtra(Intent.EXTRA_TEXT, movieDetail.homepage)
            startActivity(Intent.createChooser(i, "Share URL"))
        }

        img_save_movie.setOnClickListener {
            AlertDialog.Builder(context!!).setSingleChoiceItems(userList, -1, null)
                    .setPositiveButton("Ok") { dialog, _ ->
                        val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                        val saveMedia = buildNewSaveMedia(selectedPosition, movieDetail)

                        Log.e("XXXX", "SAVE MEDIA ID : ${saveMedia.itemId}")
                        Log.e("XXXX", "MOVIE DETA ID : ${movieDetail.id} ")

                        if (saveMedia.itemId == -1) {
                            Log.e("XXXX", "SOME ERROR SAVING DATA")
                        } else {
                            saveMovieToSelectedList(saveMedia)
                            Toast.makeText(context, "MOVIE SAVED", Toast.LENGTH_LONG).show()
                            it.setBackgroundResource(R.drawable.ic_baseline_bookmark)
                        }
                        dialog.dismiss()
                    }.setNegativeButton("Cancel") { dialog, _ ->
                        dialog.dismiss()
                    }.show()
        }

        img_rate_movie.setOnClickListener {

        }

        (activity as MainActivity).toolbar.title = movieDetail.title
    }

    private fun bindCreditsUI(credits: Credits) {
        movie_detail_cast_and_crew.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        movie_detail_cast_and_crew.adapter = CreditsAdapter(context, credits.cast)
    }

    private fun bindRecommendationsUI(movies: MutableList<Movie>) {
        rv_movie_recommendations.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = SimplePosterAdapter(context, movies)
        }
    }

    private fun saveMovieToSelectedList(saveMedia: SavedMedia) = launch {
        movieDetailFragmentViewModel.saveMovieToPlaylist(saveMedia)
    }
}