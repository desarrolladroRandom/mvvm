package com.example.mvvm.ui.view.moviedetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.models.Credits
import com.example.mvvm.models.MovieDetail
import com.example.mvvm.models.MoviesResponse
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.utils.Resource

class MovieDetailFragmentViewModel(private val theMovieDBRepository: TheMovieDBRepository) : ViewModel() {
    val movieDetail = MutableLiveData<Resource<MovieDetail>>()
    val movieDetailCredits = MutableLiveData<Resource<Credits>>()
    val movieDetailRecommendations = MutableLiveData<Resource<MoviesResponse>>()

    suspend fun updateMovieDetail(movieId: Int) {
        movieDetail.value = theMovieDBRepository.getMovieDetail(movieId)
    }

    suspend fun updateMovieDetailCredits(movieId: Int) {
        movieDetailCredits.value = theMovieDBRepository.getMovieDetailCredits(movieId)
    }

    suspend fun updateMovieDetailRecommendations(movieId: Int) {
        var recomendations = theMovieDBRepository.getMovieDetailRecommendations(movieId)
        var similar = theMovieDBRepository.getMovieDetailSimilar(movieId)
        recomendations.data.movies.addAll(similar.data.movies)
        movieDetailRecommendations.value = recomendations
    }

    suspend fun saveMovieToPlaylist(saveTvShow: SavedMedia) {
        theMovieDBRepository.saveMediaToPlaylist(saveTvShow)
    }
}