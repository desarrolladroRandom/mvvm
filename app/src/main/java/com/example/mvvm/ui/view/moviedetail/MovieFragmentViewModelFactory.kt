package com.example.mvvm.ui.view.moviedetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvm.data.TheMovieDBRepository

class MovieFragmentViewModelFactory(private val theMovieDBRepository: TheMovieDBRepository) :
        ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieDetailFragmentViewModel(theMovieDBRepository) as T
    }
}