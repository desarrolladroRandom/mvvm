package com.example.mvvm.ui.view.savedmedia

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.models.MediaItem
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.models.SearchResponse
import com.example.mvvm.models.SearchResult
import com.example.mvvm.utils.Resource

class SavedFragmentViewModel(private val theMovieDBRepository: TheMovieDBRepository) : ViewModel() {
    var savedItems = MutableLiveData<MutableList<SavedMedia>>()

    suspend fun getMediaInSelectedPlaylist(playlist: String) {
        savedItems.value = theMovieDBRepository.getMediaInSelectedPlaylist(playlist)
    }
}
