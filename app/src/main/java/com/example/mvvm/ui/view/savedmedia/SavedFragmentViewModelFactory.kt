package com.example.mvvm.ui.view.savedmedia

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvm.data.TheMovieDBRepository

class SavedFragmentViewModelFactory(private val theMovieDBRepository: TheMovieDBRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SavedFragmentViewModel(theMovieDBRepository) as T
    }
}