package com.example.mvvm.ui.view.savedmedia


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.ui.adapters.ExtendedPosterAdapter
import com.example.mvvm.utils.ScopedFragment
import com.example.mvvm.utils.userList
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_saved_list.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class SavedUserListFragment : ScopedFragment(), KodeinAware {
    override val kodein by kodein()

    private val savedFragmentViewModelFactory: SavedFragmentViewModelFactory by instance()
    private lateinit var savedFragmentViewModel: SavedFragmentViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(com.example.mvvm.R.layout.fragment_saved_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        savedFragmentViewModel = ViewModelProviders.of(this@SavedUserListFragment, savedFragmentViewModelFactory)
            .get(SavedFragmentViewModel::class.java)

        retrieveSavedMedia()

        savedFragmentViewModel.savedItems.observe(this, Observer {
            bindUI(it)
        })

        selecte_list.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                val position = tab?.position ?: 0
                retrieveSavedMedia(userList[position])
            }
        })
    }

    private fun retrieveSavedMedia(playlist: String = "Favorite") = launch {
        savedFragmentViewModel.getMediaInSelectedPlaylist(playlist)
    }

    private fun bindUI(it: MutableList<SavedMedia>) {
        rv_saved_items.apply {
            if (it.isEmpty()) {
                visibility = View.GONE
                label_savedMedia.visibility = View.VISIBLE
            } else {
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = ExtendedPosterAdapter(context, it)
                label_savedMedia.visibility = View.GONE
                visibility = View.VISIBLE
            }
        }
    }
}
