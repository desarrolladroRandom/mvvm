package com.example.mvvm.ui.view.searchscreen


import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.models.SearchResult
import com.example.mvvm.ui.adapters.ExtendedPosterAdapter
import com.example.mvvm.utils.Resource
import com.example.mvvm.utils.ScopedFragment
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.fragment_search.view.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import android.app.Activity
import android.content.Context
import android.view.*
import android.view.inputmethod.InputMethodManager


class SearchFragment : ScopedFragment(), KodeinAware {
    override val kodein by kodein()

    private val searchFragmentViewModelFactory: SearchFragmentViewModelFactory by instance()
    private lateinit var searchFragmentViewModel: SearchFragmentViewModel
    //private var searchResults = mutableListOf<SearchResult>()
    //private var searchString: String = ""
    private var searchPage: Int = 1

    companion object {
        var searchString: String = ""
        var searchResults = mutableListOf<SearchResult>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var vista = inflater.inflate(com.example.mvvm.R.layout.fragment_search, container, false)

        vista.rv_search_or_trending.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    loadMoreItems(searchString, searchPage + 1)
                }
            }
        })

        vista.editxt_search.setOnKeyListener { view: View, keyCode: Int, keyEvent: KeyEvent ->
            if ((keyEvent.action == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                searchString = editxt_search.text.toString()
                editxt_search.clearFocus()
                bindUI(searchString)
                hideKeyboard(context!!)
            }
            true
        }

        vista.btn_search.setOnClickListener {
            searchString = editxt_search.text.toString()
            editxt_search.clearFocus()
            hideKeyboard(context!!)
            bindUI(searchString)
        }

        vista.editxt_search.setText(searchString)

        vista.rv_search_or_trending.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = ExtendedPosterAdapter(context, searchResults)
        }

        return vista
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchFragmentViewModel = ViewModelProviders.of(this@SearchFragment, searchFragmentViewModelFactory)
            .get(SearchFragmentViewModel::class.java)

        initializeUI()
    }

    private fun initializeUI() = launch {
        searchFragmentViewModel.getTrendingMediaByTime("all", "week")
        searchFragmentViewModel.trendingMedia.observe(this@SearchFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> Log.e("XXXX", "Error in trending ${it.message}")
                Resource.Status.SUCCESS -> {
                    rv_search_or_trending.apply {
                        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        adapter = ExtendedPosterAdapter(context, it.data.results)
                    }
                    search_label.visibility = View.VISIBLE
                    editxt_search.setText("")
                }
            }
        })
    }

    private fun bindUI(searchString: String) = launch {
        searchFragmentViewModel.getFirstPageResults(searchString)
        searchFragmentViewModel.firstSearch.observe(this@SearchFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    Log.e("XXXX", "Error in multisearch ${it.message}")
                    rv_search_or_trending.visibility = View.GONE
                    search_label.visibility = View.VISIBLE
                    search_label.text = "No results found"
                }
                Resource.Status.SUCCESS -> {

                    searchResults = it.data.results
                    rv_search_or_trending.apply {
                        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        adapter = ExtendedPosterAdapter(context, searchResults)
                    }
                    search_label.visibility = View.GONE
                    searchPage = it.data.page

                    if (searchResults.size == 0) {
                        Log.e("XXXX", "Error in multisearch ${it.message}")
                        rv_search_or_trending.visibility = View.GONE
                        search_label.visibility = View.VISIBLE
                        search_label.text = "No results found"
                    }
                }
            }
        })
    }

    private fun loadMoreItems(searchString: String, page: Int) = launch {
        searchFragmentViewModel.getNewSearchResult(searchString, page)
        searchFragmentViewModel.newSearches.observe(this@SearchFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> Log.e("XXXX", "Error in multisearch ${it.message}")
                Resource.Status.SUCCESS -> {
                    searchResults.addAll(it.data.results)
                    rv_search_or_trending.adapter?.notifyDataSetChanged()
                }
            }
        })
    }

    private fun hideKeyboard(context: Context) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
    }
}
