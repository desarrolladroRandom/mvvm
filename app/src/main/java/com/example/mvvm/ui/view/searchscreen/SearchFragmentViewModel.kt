package com.example.mvvm.ui.view.searchscreen

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.models.SearchResponse
import com.example.mvvm.models.TrendingResponse
import com.example.mvvm.utils.Resource

class SearchFragmentViewModel(private val theMovieDBRepository: TheMovieDBRepository) : ViewModel() {
    val trendingMedia = MutableLiveData<Resource<TrendingResponse>>()
    val firstSearch = MutableLiveData<Resource<SearchResponse>>()
    val newSearches = MutableLiveData<Resource<SearchResponse>>()

    suspend fun getFirstPageResults(searchString: String) {
        firstSearch.value = theMovieDBRepository.getMultiSearch(searchString, 1)
    }

    suspend fun getNewSearchResult(searchString: String, page: Int) {
        newSearches.value = theMovieDBRepository.getMultiSearch(searchString, page)
    }

    suspend fun getTrendingMediaByTime(type: String, time: String) {
        trendingMedia.value = theMovieDBRepository.getTrendingMediaByTime(type, time)
    }
}