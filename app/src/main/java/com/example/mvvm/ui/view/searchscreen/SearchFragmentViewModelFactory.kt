package com.example.mvvm.ui.view.searchscreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvm.data.TheMovieDBRepository

class SearchFragmentViewModelFactory(private val theMovieDBRepository: TheMovieDBRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SearchFragmentViewModel(theMovieDBRepository) as T
    }
}