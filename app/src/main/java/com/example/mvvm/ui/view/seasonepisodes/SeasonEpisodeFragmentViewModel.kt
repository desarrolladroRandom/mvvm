package com.example.mvvm.ui.view.seasonepisodes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.models.*
import com.example.mvvm.utils.Resource

class SeasonEpisodeFragmentViewModel(private val theMovieDBRepository: TheMovieDBRepository) : ViewModel() {
    var seasonEpisodes = MutableLiveData<Resource<SeasonEpisodes>>()

    suspend fun getTvShowSeasonEpisode(tv_id: Int, season_number: Int) {
        seasonEpisodes.value = theMovieDBRepository.getTvShowSeasonEpisode(tv_id, season_number)
    }
}
