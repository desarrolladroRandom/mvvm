package com.example.mvvm.ui.view.seasonepisodes

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvm.data.TheMovieDBRepository

class SeasonEpisodeFragmentViewModelFactory(private val theMovieDBRepository: TheMovieDBRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SeasonEpisodeFragmentViewModel(theMovieDBRepository) as T
    }
}