package com.example.mvvm.ui.view.seasonepisodes


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.R
import com.example.mvvm.ui.adapters.EpisodeAdapter
import com.example.mvvm.utils.Resource
import com.example.mvvm.utils.ScopedFragment
import com.example.mvvm.utils.buildErro404Alert
import kotlinx.android.synthetic.main.fragment_season_episodes.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class SeasonEpisodesFragment : ScopedFragment(), KodeinAware {
    override val kodein by kodein()

    private val seasonEpisodeFragmentViewModelFactory: SeasonEpisodeFragmentViewModelFactory by instance()
    private lateinit var seasonEpisodeFragmentViewModel: SeasonEpisodeFragmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_season_episodes, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        seasonEpisodeFragmentViewModel =
            ViewModelProviders.of(this@SeasonEpisodesFragment, seasonEpisodeFragmentViewModelFactory)
                .get(SeasonEpisodeFragmentViewModel::class.java)

        val tvShowId = arguments!!.getInt("tvShowId")
        val seasonNum = arguments!!.getInt("seasonNum")

        bindUI(tvShowId, seasonNum)
    }

    private fun bindUI(tvShowId: Int, seasonNum: Int) = launch {
        seasonEpisodeFragmentViewModel.getTvShowSeasonEpisode(tvShowId, seasonNum)
        seasonEpisodeFragmentViewModel.seasonEpisodes.observe(this@SeasonEpisodesFragment, Observer {
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "ERROR ${it.message} ")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    rv_season_episodes.apply {
                        layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                        adapter = EpisodeAdapter(context, it.data.episodes)
                    }
                }
            }
        })
    }


}
