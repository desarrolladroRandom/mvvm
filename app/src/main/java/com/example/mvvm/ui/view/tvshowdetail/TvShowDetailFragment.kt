package com.example.mvvm.ui.view.tvshowdetail

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.example.mvvm.MainActivity
import com.example.mvvm.R
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.models.TvShowDetail
import com.example.mvvm.ui.adapters.TvShowDetailPagerAdapter
import com.example.mvvm.utils.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_tv_show_detail.*
import kotlinx.android.synthetic.main.fragment_tv_show_detail.view.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class TvShowDetailFragment : ScopedFragment(), KodeinAware {
    override val kodein by kodein()

    private val tvShowFragmentViewModelFactory: TvShowFragmentViewModelFactory by instance()
    private lateinit var tvShowDetailFragmentViewModel: TvShowDetailFragmentViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var vista = inflater.inflate(R.layout.fragment_tv_show_detail, container, false)
        var viewPagerAdapter = TvShowDetailPagerAdapter(childFragmentManager)
        viewPagerAdapter.addFragment(TvShowDetailTabInfo(), "Information")
        viewPagerAdapter.addFragment(TvShowDetailTabSeasons(), "Seasons")

        vista.apply {
            this.tv_show_detail_info_header.isFillViewport = true
            this.tab_layout.setupWithViewPager(view_pager)
            this.view_pager.adapter = viewPagerAdapter
        }

        val tvShowId = arguments!!.getInt("tvShow_id")
        bindUI(tvShowId)
        return vista
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tvShowDetailFragmentViewModel = ViewModelProviders.of(this, tvShowFragmentViewModelFactory)
            .get(TvShowDetailFragmentViewModel::class.java)


    }

    private fun bindUI(tvShowId: Int) = launch {
        tvShowDetailFragmentViewModel.updateTvShowDetail(tvShowId)
        tvShowDetailFragmentViewModel.tvShowDetail.observe(this@TvShowDetailFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading credits")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    TvShowDetailObj.tvShowDetailInfo.value = it.data
                    bindTvShowDetailHeaderUI(it.data)
                }
            }
        })

        tvShowDetailFragmentViewModel.updateTvShowDetailCredits(tvShowId)
        tvShowDetailFragmentViewModel.tvShowDetailCredits.observe(this@TvShowDetailFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading credits")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    TvShowDetailObj.tvShowDetailCredit.value = it.data
                }
            }
        })

        tvShowDetailFragmentViewModel.updateTvShowDetailRecommendations(tvShowId)
        tvShowDetailFragmentViewModel.tvShowDetailRecommendations.observe(this@TvShowDetailFragment, Observer {
            if (it == null) return@Observer
            when (it.status) {
                Resource.Status.ERROR -> {
                    //Log.e("XXXX", "Error loading credits")
                    buildErro404Alert(context).show()
                }
                Resource.Status.SUCCESS -> {
                    TvShowDetailObj.tvShowDetailRecommendations.value = it.data.tvShows

                    // TEMPORAL
                    loading_tv_show_detail.visibility = View.GONE
                    tv_show_detail_info_header.visibility = View.VISIBLE
                    tv_show_detail_info_view.visibility = View.VISIBLE
                }
            }
        })
    }

    private fun bindTvShowDetailHeaderUI(tvShowDetail: TvShowDetail) {
        tv_show_detail_info_title.text = tvShowDetail.name
        tv_show_detail_info_date.text = tvShowDetail.firstAirDate
        tv_show_detail_info_votes.text = "${tvShowDetail.voteCount} votes"
        tv_show_detail_info_note.text = tvShowDetail.voteAverage.toString()
        tv_show_detail_info_genres.text = tvShowDetail.genres.map { it.name }.toString()

        if (tvShowDetail.saved)
            tv_show_detail_info_img_save.setBackgroundResource(R.drawable.ic_baseline_bookmark)

        Glide.with(context!!).load("https://image.tmdb.org/t/p/w500" + tvShowDetail.posterPath)
            .into(tv_show_detail_info_poster)
        Glide.with(context!!).load("https://image.tmdb.org/t/p/w500" + tvShowDetail.backdropPath)
            .into(tv_show_detail_info_backdrop)

        tv_show_detail_info_img_share.setOnClickListener {
            val i = Intent(Intent.ACTION_SEND)
            i.type = "text/plain"
            i.putExtra(Intent.EXTRA_SUBJECT, "Echale un vistazo a esta serie :")
            i.putExtra(Intent.EXTRA_TEXT, tvShowDetail.homepage)
            startActivity(Intent.createChooser(i, "Share URL"))
        }

        tv_show_detail_info_img_save.setOnClickListener {
            AlertDialog.Builder(context!!).setSingleChoiceItems(userList, -1, null)
                .setPositiveButton("Ok") { dialog, _ ->
                    val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                    val saveMedia = buildNewSaveMedia(selectedPosition, tvShowDetail)

                    Log.e("XXXX", "SAVE MEDIA ID : ${saveMedia.itemId}")
                    Log.e("XXXX", "TVSHOW DET ID : ${tvShowDetail.id} ")

                    if (saveMedia.itemId == -1) {
                        Log.e("XXXX", "SOME ERROR SAVING DATA")
                    } else {
                        saveTvShowToSelectedList(saveMedia)
                        Toast.makeText(context, "MOVIE SAVED", Toast.LENGTH_LONG).show()
                        it.setBackgroundResource(R.drawable.ic_baseline_bookmark)
                    }
                    dialog.dismiss()
                }.setNegativeButton("Cancel") { dialog, _ ->
                    dialog.dismiss()
                }.show()
        }
        tv_show_detail_info_img_rate.setOnClickListener {

        }

        (activity as MainActivity).toolbar.title = tvShowDetail.name
    }

    private fun saveTvShowToSelectedList(saveMedia: SavedMedia) = launch {
        tvShowDetailFragmentViewModel.saveTvShowToPlaylist(saveMedia)
    }


}
