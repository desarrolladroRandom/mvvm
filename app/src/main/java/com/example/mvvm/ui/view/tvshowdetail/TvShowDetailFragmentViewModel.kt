package com.example.mvvm.ui.view.tvshowdetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mvvm.data.TheMovieDBRepository
import com.example.mvvm.models.*
import com.example.mvvm.utils.Resource

class TvShowDetailFragmentViewModel(private val theMovieDBRepository: TheMovieDBRepository) : ViewModel() {
    val tvShowDetail = MutableLiveData<Resource<TvShowDetail>>()
    val tvShowDetailCredits = MutableLiveData<Resource<Credits>>()
    val tvShowDetailRecommendations = MutableLiveData<Resource<TvShowResponse>>()

    suspend fun updateTvShowDetail(movieId: Int) {
        tvShowDetail.value = theMovieDBRepository.getTvShowDetail(movieId)
    }

    suspend fun updateTvShowDetailCredits(movieId: Int) {
        tvShowDetailCredits.value = theMovieDBRepository.getTvShowDetailCredits(movieId)
    }

    suspend fun updateTvShowDetailRecommendations(movieId: Int) {
        tvShowDetailRecommendations.value = theMovieDBRepository.getTvShowDetailRecommendations(movieId)
    }

    suspend fun saveTvShowToPlaylist(saveTvShow: SavedMedia) {
        theMovieDBRepository.saveMediaToPlaylist(saveTvShow)
    }
}