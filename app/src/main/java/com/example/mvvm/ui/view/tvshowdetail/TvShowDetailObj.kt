package com.example.mvvm.ui.view.tvshowdetail

import androidx.lifecycle.MutableLiveData
import com.example.mvvm.models.Credits
import com.example.mvvm.models.TvShow
import com.example.mvvm.models.TvShowDetail

object TvShowDetailObj {
    var tvShowDetailInfo = MutableLiveData<TvShowDetail>()
    var tvShowDetailCredit = MutableLiveData<Credits>()
    var tvShowDetailRecommendations = MutableLiveData<MutableList<TvShow>>()
}