package com.example.mvvm.ui.view.tvshowdetail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.R
import com.example.mvvm.models.Cast
import com.example.mvvm.models.Credits
import com.example.mvvm.models.TvShow
import com.example.mvvm.models.TvShowDetail
import com.example.mvvm.ui.adapters.CreditsAdapter
import com.example.mvvm.ui.adapters.SimplePosterAdapter
import kotlinx.android.synthetic.main.fragment_tv_show_detail_info.*

class TvShowDetailTabInfo : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tv_show_detail_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        TvShowDetailObj.tvShowDetailInfo.observe(this, Observer {
            bindTvShowDetailInfoUI(it)
        })
        TvShowDetailObj.tvShowDetailCredit.observe(this, Observer {
            bindTvShowDetailCreditsUI(it)
        })
        TvShowDetailObj.tvShowDetailRecommendations.observe(this, Observer {
            bindTvShowDetailRecommendationsUI(it)
        })
    }

    private fun bindTvShowDetailInfoUI(tvShowDetail: TvShowDetail) {
        tv_show_detail_info_overview.text = tvShowDetail.overview
        tv_show_detail_info_duration.text = "${tvShowDetail.episodeRunTime.average().toInt()} min."
        tv_show_detail_info_homepage.text = tvShowDetail.homepage
        tv_show_detail_info_num_episodes.text = tvShowDetail.numberOfEpisodes.toString()
        tv_show_detail_info_num_seasons.text = tvShowDetail.numberOfSeasons.toString()

        tv_show_detail_info_homepage.setOnClickListener {
            var i = Intent(Intent.ACTION_VIEW, Uri.parse(tvShowDetail.homepage))
            startActivity(i)
        }

        if (tvShowDetail.createdBy.isNotEmpty()) {
            var creators = tvShowDetail.createdBy.map {
                Cast(name = it.name, profilePath = if (it.profilePath == null) "" else it.profilePath)
            }.toMutableList()
            rv_show_detail_info_created_by.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            rv_show_detail_info_created_by.adapter = CreditsAdapter(context, creators)
        } else {
            tv_show_detail_title_created_by.visibility = View.GONE
            rv_show_detail_info_created_by.visibility = View.GONE
        }
    }

    private fun bindTvShowDetailCreditsUI(credits: Credits) {
        rv_show_detail_info_cast_and_crew.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        rv_show_detail_info_cast_and_crew.adapter = CreditsAdapter(context, credits.cast)
    }

    private fun bindTvShowDetailRecommendationsUI(tvShows: MutableList<TvShow>) {
        rv_tv_show_detail_info_recommendations.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = SimplePosterAdapter(context, tvShows)
        }
    }
}
