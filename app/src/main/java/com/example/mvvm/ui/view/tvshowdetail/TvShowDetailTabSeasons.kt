package com.example.mvvm.ui.view.tvshowdetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.R
import com.example.mvvm.models.Season
import com.example.mvvm.ui.adapters.SeasonAdapter
import kotlinx.android.synthetic.main.fragment_tv_show_detail_seasons.*

class TvShowDetailTabSeasons : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tv_show_detail_seasons, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        TvShowDetailObj.tvShowDetailInfo.observe(this, Observer {
            bindTvShowDetailSeasonsUI(it.seasons)
        })
    }

    private fun bindTvShowDetailSeasonsUI(seasons: MutableList<Season>) {
        tv_show_detail_info_seasons.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = SeasonAdapter(context, seasons)
        }
    }
}
