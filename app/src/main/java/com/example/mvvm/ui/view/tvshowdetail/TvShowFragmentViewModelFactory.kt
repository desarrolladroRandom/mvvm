package com.example.mvvm.ui.view.tvshowdetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvm.data.TheMovieDBRepository

class TvShowFragmentViewModelFactory(private val theMovieDBRepository: TheMovieDBRepository) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return TvShowDetailFragmentViewModel(theMovieDBRepository) as T
    }
}