package com.example.mvvm.utils

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import android.content.DialogInterface
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatDialogFragment
import com.example.mvvm.MainActivity
import com.example.mvvm.R

fun buildNoConnectionAlert(context: Context?):AlertDialog {
    var builder = AlertDialog.Builder(context!!)
    var inflater = (context as MainActivity).layoutInflater
    var view = inflater.inflate(R.layout.alert_error_no_connection, null)
    return builder.setView(view).create()
}

fun buildErro404Alert(context: Context?):AlertDialog {
    var builder = AlertDialog.Builder(context!!)
    var inflater = (context as MainActivity).layoutInflater
    var view = inflater.inflate(R.layout.alert_error_404, null)
    return builder.setView(view).create()
}

fun buildSomethingWrong(context: Context?):AlertDialog {
    var builder = AlertDialog.Builder(context!!)
    var inflater = (context as MainActivity).layoutInflater
    var view = inflater.inflate(R.layout.alert_error_404, null)
    return builder.setView(view).create()
}