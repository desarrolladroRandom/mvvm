package com.example.mvvm.utils

import com.example.mvvm.models.MediaItem
import com.example.mvvm.models.MovieDetail
import com.example.mvvm.models.SavedMedia
import com.example.mvvm.models.TvShowDetail

var userList = arrayOf("Favorite", "Pending", "Seen")

fun getGenreIdToString(genreId: Int): String {
    return when (genreId) {
        10759 -> "Action & Adventure"
        16 -> "Animation"
        35 -> "Comedy"
        80 -> "Crime"
        99 -> "Documentary"
        18 -> "Drama"
        10751 -> "Family"
        10762 -> "Kids"
        9648 -> "Mystery"
        10763 -> "News"
        10764 -> "Reality"
        10765 -> "Sci-Fi & Fantasy"
        10766 -> "Soap"
        10767 -> "Talk"
        10768 -> "War & Politics"
        37 -> "Western"
        28 -> "Action"
        12 -> "Adventure"
        14 -> "Fantasy"
        36 -> "History"
        27 -> "Horror"
        10402 -> "Music"
        10749 -> "Romance"
        878 -> "Science Fiction"
        10770 -> "TV Movie"
        53 -> "Thriller"
        10752 -> "War"
        else -> ""
    }
}

fun <T> buildNewSaveMedia(selectedListPos: Int, mediaToSave: T): SavedMedia {
    when (mediaToSave) {
        is TvShowDetail -> {
            var tvShowToSave = MediaItem(
                adult = false,
                backdropPath = mediaToSave.backdropPath,
                firstAirDate = mediaToSave.firstAirDate,
                genreIds = mediaToSave.genres.map { it.id }.toMutableList(),
                id = mediaToSave.id,
                mediaType = "tv",
                name = mediaToSave.name,
                originCountry = mediaToSave.originCountry,
                originalLanguage = mediaToSave.originalLanguage,
                originalName = mediaToSave.originalName,
                originalTitle = mediaToSave.originalName,
                overview = mediaToSave.overview,
                popularity = mediaToSave.popularity,
                posterPath = mediaToSave.posterPath,
                releaseDate = mediaToSave.firstAirDate,
                title = mediaToSave.name,
                voteAverage = mediaToSave.voteAverage,
                voteCount = mediaToSave.voteCount
            )
            return SavedMedia(mediaToSave.id, userList[selectedListPos], tvShowToSave)
        }
        is MovieDetail -> {
            var movieToSave = MediaItem(
                adult = mediaToSave.adult,
                backdropPath = mediaToSave.backdropPath,
                firstAirDate = "",
                genreIds = mediaToSave.genres.map { it.id }.toMutableList(),
                id = mediaToSave.id,
                mediaType = "movie",
                name = mediaToSave.title,
                originCountry = mutableListOf(""),
                originalLanguage = mediaToSave.originalLanguage,
                originalName = "",
                originalTitle = mediaToSave.originalTitle,
                overview = mediaToSave.overview,
                popularity = mediaToSave.popularity,
                posterPath = mediaToSave.posterPath,
                releaseDate = mediaToSave.releaseDate,
                title = mediaToSave.title,
                voteAverage = mediaToSave.voteAverage,
                voteCount = mediaToSave.voteCount
            )
            return SavedMedia(mediaToSave.id, userList[selectedListPos], movieToSave)
        }
        else -> return SavedMedia(-1, userList[selectedListPos], null)
    }
}
