# What2Watch app
This app uses TheMovieDb API https://www.themoviedb.org/ to show list of movies and tvShows.

# This app uses architectural components and the following technologies:
   - MVVM pattern
   - Reftrofit
   - Glide for image loading
   - Livedata
   - Room for local database
   - Kodein for dependency injection
   - Kotlin Coroutines
# Features
   - Save TvShows to Favorite/Pending/Seen
   - Share TvShows and Movies 
   - Rate TvShows and Movies (in process)

# App images
##### Home page
![home](images/home.jpg =150x75)

##### Movie detail
![moviedetail](images/moviedetail.jpg =150x75)

##### Movie detail recommendations
![movierecomendation](images/movierecomendation.jpg =150x75)

##### TvShow detail
![tvshowdetail](images/tvshowdetail.jpg =150x75)

##### TvShow detail recommendations
![tvshowrecomendation](images/tvshowrecomendation.jpg =150x75)

##### TvShow detail seasons
![tvshowseason](images/tvshowseason.jpg =150x75)

##### TvShow detail season episodes
![seasonepisodes](images/seasonepisodes.jpg =150x75)

##### Saved lists page
![savedlist](images/savedlist.jpg =150x75)

##### Search  page
![search](images/search.jpg =150x75)



